

ActiveThorns = "CoordBase SymBase Boundary NanChecker CartGrid3d Time MoL CarpetIOBasic CarpetIOScalar IOUtil Carpet CarpetLib CarpetReduce CarpetInterp CarpetSlab CarpetIOASCII ADMBase StaticConformal SpaceMask Slab Periodic Exact GenericFD CoordGauge ADMCoupling LoopControl ML_BSSN ML_BSSN_Helper GenericFD TMuNuBase SphericalSurface ADMMacros TimerReport"

ADMBase::evolution_method         = "ML_BSSN"
ADMBase::lapse_evolution_method   = "ML_BSSN"
ADMBase::shift_evolution_method   = "ML_BSSN"
ADMBase::dtlapse_evolution_method = "ML_BSSN"
ADMBase::dtshift_evolution_method = "ML_BSSN"

ML_BSSN::harmonicN           = 1      # 1+log
ML_BSSN::harmonicF           = 2.0    # 1+log
ML_BSSN::ShiftGammaCoeff     = 0.75
ML_BSSN::BetaDriver          = 1.0
ML_BSSN::LapseAdvectionCoeff = 1.0
ML_BSSN::ShiftAdvectionCoeff = 1.0

ML_BSSN::MinimumLapse              = 1.0e-8
ML_BSSN::conformalMethod           = 1 # 1 for W, 0 for phi
ML_BSSN::my_rhs_boundary_condition = "static"
ML_BSSN::apply_dissipation         = "never"
ML_BSSN::fdOrder                   = 8

Boundary::radpower                     = 2

#############################################################
# Grid
#############################################################

CoordBase::domainsize                   = minmax

CoordBase::boundary_size_x_lower        = 5
CoordBase::boundary_size_y_lower        = 5
CoordBase::boundary_size_z_lower        = 5
CoordBase::boundary_shiftout_x_lower    = 1
CoordBase::boundary_shiftout_y_lower    = 1
CoordBase::boundary_shiftout_z_lower    = 1

CoordBase::boundary_size_x_upper        = 5
CoordBase::boundary_size_y_upper        = 5
CoordBase::boundary_size_z_upper        = 5
CoordBase::boundary_shiftout_x_upper    = 0
CoordBase::boundary_shiftout_y_upper    = 0
CoordBase::boundary_shiftout_z_upper    = 0

CartGrid3D::type                        = "coordbase"
CartGrid3D::domain                      = "full"
CartGrid3D::avoid_origin                = "no"

Periodic::periodic = "yes"

CoordBase::xmin                         = 0
CoordBase::ymin                         = 0
CoordBase::zmin                         = 0

CoordBase::xmax                         = 1
CoordBase::ymax                         = 1
CoordBase::zmax                         = 1

CoordBase::dx                           = 0.02
CoordBase::dy                           = 0.02
CoordBase::dz                           = 0.02

#############################################################
# Carpet
#############################################################

Carpet::ghost_size                      = 5
Carpet::domain_from_coordbase           = "yes"
Carpet::init_3_timelevels               = "no"
Carpet::poison_new_timelevels           = yes



#############################################################
# Timers
#############################################################

TimerReport::output_all_timers_readable = yes
TimerReport::n_top_timers               = 40
TimerReport::output_schedule_timers     = no
Carpet::output_timer_tree_every         = 1
Carpet::output_initialise_timer_tree    = yes

#############################################################
# Time integration
#############################################################

Cactus::terminate                         = "iteration"
Cactus::cctk_itlast                       = 10

Time::dtfac                           = 0.5

MethodOfLines::ode_method             = "RK4"
MethodOfLines::MoL_Intermediate_Steps = 4

MethodOfLines::MoL_Num_Scratch_Levels = 1
MethodOfLines::MoL_NaN_Check          = "no"
MethodOfLines::initial_data_is_crap   = "no"

#############################################################
# Initial data
#############################################################

ADMBase::initial_data = "Cartesian Minkowski"
ADMBase::initial_lapse = "one"
ADMBase::initial_shift = "zero"
ADMBase::initial_dtlapse = "zero"
ADMBase::initial_dtshift = "zero"

#############################################################
# Output
#############################################################

IO::out_dir                   = $parfile
IO::out_fileinfo              = "none"
IO::parfile_write             = "no"
CarpetIOBasic::outInfo_every        = 1
CarpetIOBasic::outInfo_vars         = "ADMBase::alp"

IOASCII::out0D_every                    = 10
IOASCII::out0D_vars                     = "Carpet::timing"
IOASCII::one_file_per_group             = yes

IOASCII::out1D_every          = 0
IOASCII::out_precision        = 19
IOASCII::out1D_x              = "yes"
IOASCII::out1D_y              = "no"
IOASCII::out1D_z              = "no"
IOASCII::out1D_d              = "no"
IOASCII::out1D_vars           = "ML_BSSN::ML_curvrhs ML_BSSN::ML_dtlapserhs ML_BSSN::ML_dtshiftrhs ML_BSSN::ML_Gammarhs ML_BSSN::ML_lapserhs ML_BSSN::ML_log_confacrhs ML_BSSN::ML_metricrhs ML_BSSN::ML_shiftrhs ML_BSSN::ML_trace_curvrhs"
