#!/usr/bin/env python

import sys
import re
from string import Template

files = [{'order' : 4}]

for f in files:

    order = f['order']

    stencil = order/2+1

    filename = "ord%s" % order

    filename += ".par"
        
    lines = """

ActiveThorns = "CoordBase SymBase Boundary NanChecker CartGrid3d Time MoL CarpetIOBasic CarpetIOScalar IOUtil Carpet CarpetLib CarpetReduce CarpetInterp CarpetSlab CarpetIOASCII ADMBase StaticConformal SpaceMask Slab Periodic Exact GenericFD CoordGauge ADMCoupling LoopControl SphericalSurface ADMMacros TMuNuBase BSSN_MoL SphericalSurface ADMMacros TimerReport"

ADMBase::evolution_method               = "adm_bssn"

ADMBase::lapse_evolution_method         = "1+log"
ADM_BSSN::lapsesource                   = "straight"
ADM_BSSN::harmonic_f                    = 2.0
ADM_BSSN::force_lapse_positive          = yes
ADM_BSSN::lapse_advection_coeff         = 1.0

ADMBase::shift_evolution_method         = "gamma0"
ADM_BSSN::ShiftGammaCoeff               = 0.75
ADM_BSSN::BetaDriver                    = 1.0
ADM_BSSN::gamma_driver_advection_coeff  = 1.0
ADM_BSSN::ApplyShiftBoundary            = yes

ADM_BSSN::bound                         = "newrad"

ADMMacros::spatial_order = 4
ADM_BSSN::stencil_size   = 3
ADM_BSSN::timelevels     = 3
ADM_BSSN::advection      = "upwind4"
ADM_BSSN::constraints    = yes

Boundary::radpower                     = 2

#############################################################
# Grid
#############################################################

CoordBase::domainsize                   = minmax

CoordBase::boundary_size_x_lower        = $stencil
CoordBase::boundary_size_y_lower        = $stencil
CoordBase::boundary_size_z_lower        = $stencil
CoordBase::boundary_shiftout_x_lower    = 1
CoordBase::boundary_shiftout_y_lower    = 1
CoordBase::boundary_shiftout_z_lower    = 1

CoordBase::boundary_size_x_upper        = $stencil
CoordBase::boundary_size_y_upper        = $stencil
CoordBase::boundary_size_z_upper        = $stencil
CoordBase::boundary_shiftout_x_upper    = 0
CoordBase::boundary_shiftout_y_upper    = 0
CoordBase::boundary_shiftout_z_upper    = 0

CartGrid3D::type                        = "coordbase"
CartGrid3D::domain                      = "full"
CartGrid3D::avoid_origin                = "no"

Periodic::periodic = "yes"

CoordBase::xmin                         = 0
CoordBase::ymin                         = 0
CoordBase::zmin                         = 0

CoordBase::xmax                         = 1
CoordBase::ymax                         = 1
CoordBase::zmax                         = 1

CoordBase::dx                           = 0.02
CoordBase::dy                           = 0.02
CoordBase::dz                           = 0.02

#############################################################
# Carpet
#############################################################

Carpet::ghost_size                      = $stencil
Carpet::domain_from_coordbase           = "yes"
Carpet::init_3_timelevels               = "no"
Carpet::poison_new_timelevels           = yes

# This is needed to get constraints for the BSSN constraint variables
Carpet::enable_all_storage              = yes

#############################################################
# Timers
#############################################################

TimerReport::output_all_timers_readable = yes
TimerReport::n_top_timers               = 40
TimerReport::output_schedule_timers     = no
Carpet::output_timer_tree_every         = 1
Carpet::output_initialise_timer_tree    = yes

#############################################################
# Time integration
#############################################################

Cactus::terminate                         = "iteration"
Cactus::cctk_itlast                       = 10

Time::dtfac                           = 0.5

MethodOfLines::ode_method             = "RK4"
MethodOfLines::MoL_Intermediate_Steps = 4

MethodOfLines::MoL_Num_Scratch_Levels = 1
MethodOfLines::MoL_NaN_Check          = "no"
MethodOfLines::initial_data_is_crap   = "no"

#############################################################
# Initial data
#############################################################

ADMBase::initial_data = "Cartesian Minkowski"
ADMBase::initial_lapse = "one"
ADMBase::initial_shift = "zero"
ADMBase::initial_dtlapse = "zero"
ADMBase::initial_dtshift = "zero"

#############################################################
# Output
#############################################################

IO::out_dir                   = $$$$parfile
IO::out_fileinfo              = "none"
IO::parfile_write             = "no"
CarpetIOBasic::outInfo_every        = 1
CarpetIOBasic::outInfo_vars         = "ADMBase::alp"

IOASCII::out0D_every                    = 10
IOASCII::out0D_vars                     = "Carpet::timing"
IOASCII::one_file_per_group             = yes

IOASCII::out1D_every          = 1
IOASCII::out_precision        = 19
IOASCII::out1D_x              = "yes"
IOASCII::out1D_y              = "no"
IOASCII::out1D_z              = "no"
IOASCII::out1D_d              = "no"
IOASCII::out1D_vars           = "ADM_BSSN::ADM_BSSN_cons" # Trigger the constraints
"""

    open(filename, 'w').write(re.sub(r'\n *',r'\n',Template(Template(lines).substitute(locals())).substitute(locals())))
