<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/suite">
    <html>
      <head>
        <title>Benchmark suite</title>
        <link href="timers.css" rel="stylesheet" type="text/css"/>
        </head>
      <body>
        <h1>Benchmark suite</h1>
        <xsl:for-each select="set">
          <h2><xsl:value-of select="name"/></h2>
          <xsl:for-each select="benchmark">
            <h2><xsl:value-of select="name"/></h2>
            <table>
              <tr>
                <th class="date">Timer</th>
                <th>Time (s)</th>
              </tr>
              <xsl:for-each select="timers/timer">
                <tr>
                  <td><xsl:value-of select="name"/></td>
                  <td><xsl:value-of select="value"/></td>
                </tr>
              </xsl:for-each>
            </table>
          </xsl:for-each>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
