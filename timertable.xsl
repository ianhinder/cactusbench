<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/timertable">
    <html>
      <head>
        <title>Benchmark results</title>
        <link href="timers.css" rel="stylesheet" type="text/css"/>
        </head>
      <body>
        <h1>Benchmark results</h1>

        <table>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <xsl:for-each select="suitename">
              <th><xsl:value-of select="."/></th>
            </xsl:for-each>
          </tr>

          <xsl:for-each select="set">
            <xsl:for-each select="benchmark">
              <xsl:for-each select="timer">
                <tr>
                  <xsl:if test="count(parent::*/preceding-sibling::benchmark) + 1 = 1 and position() = 1">
                    <td rowspan="{count(../../benchmark) * count(../timer)}" class="topalign">
                      <xsl:value-of select="../../name"/>
                    </td>
                  </xsl:if>
                  <xsl:if test="position() = 1">
                    <td rowspan="{count(../timer)}" class="topalign">
                      <xsl:value-of select="../name"/>
                    </td>
                  </xsl:if>
                  <td>
                  <xsl:value-of select="name"/></td>
                  <xsl:for-each select="value">
                    <td><xsl:value-of select="."/></td>
                  </xsl:for-each>
                </tr>
              </xsl:for-each>
            </xsl:for-each>
          </xsl:for-each>
        </table>          
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
